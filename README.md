# KaggleCompetition

## Authors
* **Mariane Schneider**
* **Saba Daneshgar**

## Name
This script is created as a starting point for you for the Kaggle Competition (https://www.kaggle.com/competitions/dynamic-modeling-of-wastewater-treatment-process) which is initiated by the IWA working group on hybrid modelling in water and wastewater treatment (https://www.linkedin.com/posts/hybrid-modelling-working-group_hybridmodeling-watertreatment-wastewatertreatment).

## Description
This script can be used to model the Tilburg wastewater treatment plant with the activated sludge model numer 1 (ASM1). Please check the Kaggle Competition for details. Please keep in mind that this is an uncalibrated model.

## Installation
1. clone the repository.
2. install the requirements `pip install -r requirements.txt`
3. install PeePyPoo by following the instructions here: https://datinfo.gitlab.io/PeePyPoo/ (we also recommend to use the virtual environment.
4. run `s_competition.py`

## Support
If you have questions, please contact us via the Forum of the Kaggle Competition (https://www.kaggle.com/competitions/dynamic-modeling-of-wastewater-treatment-process/discussion) or raise an issue here on the repository.

## Contributing
We are happy about contributions. Please get in contact with us.

## Acknowledgment
https://gitlab.com/datinfo/PeePyPoo

## License
GNU General Public License

## Project status
This is a very activ project. Please feel free to get in touch with us to report bugs and make suggestions. However, also keep in mind that the version you get is exactly a basis for a mechanistic model that is also your goal in the competition to improve.
